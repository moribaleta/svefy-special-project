package xyz.ceberus.svefy;/*
package xyz.ceberus.portifystock;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Portfolio {
	ArrayList<Company>arrList_Company;
	
	BigDecimal bdPortfolioReturn;
	BigDecimal bdPortfolioVariance;
	
	int intMaxStock;
	public Portfolio(int intMaxStock) {
		arrList_Company = new ArrayList<Company>();
		this.intMaxStock = intMaxStock;
		// TODO Auto-generated constructor stub
	}
	
	public Portfolio(ArrayList<Company>arrList_Company,int intMaxStock){
		this.arrList_Company = arrList_Company;
		this.intMaxStock = intMaxStock;
	}
	
	public Company get(int index){
		return arrList_Company.get(index);
	}
	
	public void set(int index, Company company){
		arrList_Company.set(index, company);
	}
	
	public void add(Company company){
		arrList_Company.add(company);
	}
	
	public int getSize(){
		return arrList_Company.size();
	}
	
	public BigDecimal getPortfolioReturn(){
		BigDecimal bdReturnValue = new BigDecimal(0);
		bdReturnValue.setScale(5, RoundingMode.CEILING);
		BigDecimal bdTotalWeight = getTotalWeight();
		for(Company compOut: arrList_Company){
			BigDecimal bdWeight = compOut.getCurrPrice().divide(bdTotalWeight,MathContext.DECIMAL32);
			BigDecimal bdReturnAve = compOut.getReturnAve();
			try{
				bdReturnValue = bdReturnValue.add(bdWeight.multiply(bdReturnAve));
			} catch (ArithmeticException e) {		
				System.out.println(e);
		    }
			//System.out.println(bdReturnValue);
		}		
		bdPortfolioReturn = bdReturnValue.setScale(5, RoundingMode.CEILING);
		return bdReturnValue;
	}

	private BigDecimal getTotalWeight() {
		BigDecimal bdTotal = new BigDecimal(0);
		bdTotal.setScale(5, RoundingMode.CEILING);
		for(Company compOut: arrList_Company){
			bdTotal = bdTotal.add(compOut.getCurrPrice().multiply(new BigDecimal(intMaxStock)));			
		}
		return bdTotal;
	}
	
	public BigDecimal getWeight(String strCode){
		BigDecimal bdTotalWeight = getTotalWeight();
		for(Company compOut: arrList_Company){
			if(compOut.strCode==strCode){
				return ((compOut.getCurrPrice().multiply(new BigDecimal(intMaxStock))).divide(bdTotalWeight,MathContext.DECIMAL32));
			}
		}		
		return null;		
	}
	
	public BigDecimal getPortfolioFitess(){
		getPortfolioVariance();
		getPortfolioReturn();		
		double dbVarianceValue = bdPortfolioVariance.doubleValue();				
		return bdPortfolioReturn.divide(new BigDecimal(Math.sqrt(dbVarianceValue)),MathContext.DECIMAL32).multiply(new BigDecimal(100), MathContext.DECIMAL32).setScale(5, RoundingMode.CEILING);
	}
	
	public BigDecimal getPortfolioFitess(boolean bl){			
		double dbVarianceValue = bdPortfolioVariance.doubleValue();				
		return bdPortfolioReturn.divide(new BigDecimal(Math.sqrt(dbVarianceValue)),MathContext.DECIMAL32).setScale(5, RoundingMode.CEILING);
	}
		
	public BigDecimal getPortfolioVariance(){
		BigDecimal	bdDecimal = new BigDecimal(0);
		
		for(Company compOut:arrList_Company){
			bdDecimal =  
					bdDecimal.add(
							getWeight(compOut.strCode).pow(2)
							.multiply(compOut.getVariance().pow(2))
							);			
		}
		
		BigDecimal bdSum = new BigDecimal(0);
		
		for(int i =0; i< arrList_Company.size();i++){
			BigDecimal bdSumSum = new BigDecimal(0);
			for(int j=1; j<arrList_Company.size();j++){
				bdSumSum = bdSumSum.add(
						(getWeight(arrList_Company.get(i).strCode).multiply(getWeight(arrList_Company.get(j).strCode)))
						.multiply(cov(arrList_Company.get(i),arrList_Company.get(j)))
						.multiply(new BigDecimal(2))
						);						
			}			
			bdSum = bdSum.add(bdSumSum);
		}				
		bdPortfolioVariance = bdDecimal.add(bdSum);
		return bdPortfolioVariance.setScale(5, RoundingMode.CEILING);		
	}

	private BigDecimal cov(Company compA, Company compB) {
		// TODO Auto-generated method stub
		int size = compA.arrList_Stock.size();
		BigDecimal bdCov = new BigDecimal(0);
		for(int i = size-1; i>0; i--){
			bdCov =	bdCov.add(
					compA.getReturn(i).subtract(
							compA.getReturnAve()
							)
					.multiply(
							(
									compB.getReturn(i).subtract(
											compB.getReturnAve()
											)
									)
							)
					);
		}
		
		return bdCov.divide(new BigDecimal(size-1),MathContext.DECIMAL32);
	}
	
	
	
	
}
*/
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Portfolio {
	//----------- global variables ----------------
	ArrayList<Company>arrList_Company;
	BigDecimal bdPortfolioReturn;
	BigDecimal bdPortfolioVariance;
	BigDecimal bdPortfolioFitness;
	BigDecimal bdTotalWeight;
	int intMaxStock;
	//----------- global variables ----------------

	//----------- constructors --------------------
	public Portfolio(int intMaxStock) {
		arrList_Company = new ArrayList<Company>();
		this.intMaxStock = intMaxStock;
		// TODO Auto-generated constructor stub
	}

	public Portfolio(ArrayList<Company>arrList_Company,int intMaxStock){
		this.arrList_Company = arrList_Company;
		this.intMaxStock = intMaxStock;
		bdTotalWeight = getTotalWeight();
		computePortfolioFitness();
	}
	//----------- constructors --------------------

	//----------- arrList_Company manipulators ----
	public Company get(int index){
		return arrList_Company.get(index);
	}

	public void set(int index, Company company){
		arrList_Company.set(index, company);
	}

	public void add(Company company){
		arrList_Company.add(company);
	}

	public int getSize(){
		return arrList_Company.size();
	}
	//----------- arrList_Company manipulators ----

	//----------- fitness function ----------------
	public void computePortfolioFitness(){
		bdTotalWeight = getTotalWeight();
		getPortfolioVariance();
		getPortfolioReturn();
		//double dbVarianceValue = bdPortfolioVariance.doubleValue();
		bdPortfolioFitness = bdPortfolioReturn.divide(new BigDecimal(Math.sqrt(bdPortfolioVariance.doubleValue())),MathContext.DECIMAL32).multiply(new BigDecimal(100), MathContext.DECIMAL32).setScale(5, RoundingMode.CEILING);
	}
	/*public BigDecimal getPortfolioFitess(boolean bl){
		double dbVarianceValue = bdPortfolioVariance.doubleValue();
		return bdPortfolioReturn.divide(new BigDecimal(Math.sqrt(dbVarianceValue)),MathContext.DECIMAL32).setScale(5, RoundingMode.CEILING);
	}*/
	//----------- fitness function ----------------


	public BigDecimal getPortfolioReturn(){
		BigDecimal bdReturnValue = new BigDecimal(0);
		bdReturnValue.setScale(5, RoundingMode.CEILING);
		BigDecimal bdTotalWeight = getTotalWeight();
		for(Company compOut: arrList_Company){
			BigDecimal bdWeight = compOut.getCurrPrice().divide(bdTotalWeight,MathContext.DECIMAL32);
			BigDecimal bdReturnAve = compOut.getReturnAve();
			try{
				bdReturnValue = bdReturnValue.add(bdWeight.multiply(bdReturnAve));
			} catch (ArithmeticException e) {
				System.out.println(e);
			}
			//System.out.println(bdReturnValue);
		}
		bdPortfolioReturn = bdReturnValue.setScale(5, RoundingMode.CEILING);
		return bdReturnValue;
	}

	private BigDecimal getTotalWeight() {
		BigDecimal bdTotal = new BigDecimal(0);
		bdTotal.setScale(5, RoundingMode.CEILING);
		for(Company compOut: arrList_Company){
			bdTotal = bdTotal.add(compOut.getCurrPrice().multiply(new BigDecimal(intMaxStock)));
		}
		return bdTotal;
	}

	public BigDecimal getWeight(String strCode){
		//BigDecimal bdTotalWeight = getTotalWeight();
		BigDecimal bdTotalWeight = this.bdTotalWeight;
		for(Company compOut: arrList_Company){
			if(compOut.strCode==strCode){
				return ((compOut.getCurrPrice().multiply(new BigDecimal(intMaxStock))).divide(bdTotalWeight,MathContext.DECIMAL32));
			}
		}
		return null;
	}




	public BigDecimal getPortfolioVariance(){
		BigDecimal	bdDecimal = new BigDecimal(0);

		for(Company compOut:arrList_Company){
			bdDecimal =
					bdDecimal.add(
							getWeight(compOut.strCode).pow(2)
									.multiply(compOut.getVariance().pow(2))
					);
		}

		BigDecimal bdSum = new BigDecimal(0);

		for(int i =0; i< arrList_Company.size();i++){
			BigDecimal bdSumSum = new BigDecimal(0);
			for(int j=1; j<arrList_Company.size();j++){
				bdSumSum = bdSumSum.add(
						(getWeight(arrList_Company.get(i).strCode).multiply(getWeight(arrList_Company.get(j).strCode)))
								.multiply(cov(arrList_Company.get(i),arrList_Company.get(j)))
								.multiply(new BigDecimal(2))
				);
			}
			bdSum = bdSum.add(bdSumSum);
		}
		bdPortfolioVariance = bdDecimal.add(bdSum);
		return bdPortfolioVariance.setScale(5, RoundingMode.CEILING);
	}

	private BigDecimal cov(Company compA, Company compB) {
		// TODO Auto-generated method stub
		int size = compA.arrList_Stock.size();
		BigDecimal bdCov = new BigDecimal(0);
		for(int i = size-1; i>0; i--){
			bdCov =	bdCov.add(
					compA.getReturn(i).subtract(
							compA.getReturnAve()
					)
							.multiply(
									(
											compB.getReturn(i).subtract(
													compB.getReturnAve()
											)
									)
							)
			);
		}

		return bdCov.divide(new BigDecimal(size-1),MathContext.DECIMAL32);
	}




}

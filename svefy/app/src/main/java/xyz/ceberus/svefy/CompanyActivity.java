package xyz.ceberus.svefy;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;


public class CompanyActivity extends AppCompatActivity {

    private static final String TAG = "CompanyActivity";
    LinearLayout linearLayout_data;
    DBHandler dbHandler;
    Boolean blChanged = false;
    Boolean blSaved = false;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final String strCode = getIntent().getStringExtra("Company_Code");

        linearLayout_data = (LinearLayout)findViewById(R.id.linear_layout_data);

        dbHandler = new DBHandler(this);
        dbHandler.createDatabase();
        dbHandler.open();

        Portfolio portfolio = dbHandler.getPortfolio();
        Company company = dbHandler.getCompany(strCode);
        for(Company compOut: portfolio.arrList_Company){
            if(compOut.strCode.equals(strCode))
                blSaved = true;
        }

        Log.d(TAG,"contained in portfolio"+(portfolio.arrList_Company.contains(company)));

        setTitle(company.strName);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHandler.open();
                if(blSaved){
                    dbHandler.deleteFromPortfolio(strCode);
                    Snackbar.make(view, "removed from portfolio", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    blSaved = false;
                }else {
                    dbHandler.saveToPortfolio(strCode);
                    Snackbar.make(view, "added to portfolio", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    blSaved = true;
                }
                dbHandler.close();
                blChanged = true;
                setIcon();
            }
        });
        setIcon();
        inflateLayout(company);
        dbHandler.close();
    }

    public void setIcon(){
        if(blSaved){
            fab.setImageResource(android.R.drawable.ic_delete);
        }else{
            fab.setImageResource(android.R.drawable.btn_star);
        }
    }

    private void inflateLayout(Company company) {
        BigDecimal bdReturn = company.getReturnAve();
        BigDecimal bdVariance = company.getVariance();

        TextView textViewReturn = (TextView)findViewById(R.id.textViewReturn);
        TextView textViewVariance = (TextView)findViewById(R.id.textViewVariance);

        textViewReturn.append(bdReturn.multiply(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_CEILING)+"%");
        textViewVariance.append(bdVariance.setScale(5,BigDecimal.ROUND_CEILING)+"");

        String strCode = company.strCode;
        ArrayList<Stock> arrayList_Stock = dbHandler.getStockDataFrom(strCode);
        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(Stock stockOut: arrayList_Stock) {
            View view = li.inflate(R.layout.stockdata, null);
            TextView txtViewDate = (TextView)view.findViewById(R.id.textViewDate);
            TextView txtViewOpen = (TextView)view.findViewById(R.id.textViewOpen);
            TextView txtViewClose = (TextView)view.findViewById(R.id.textViewClose);
            TextView txtViewHigh = (TextView)view.findViewById(R.id.textViewHigh);
            TextView txtViewLow = (TextView)view.findViewById(R.id.textViewLow);
            TextView txtViewValue = (TextView)view.findViewById(R.id.textViewValue);
            txtViewDate.append(" "+stockOut.strDate);
            txtViewOpen.append(" "+stockOut.bdOpen.setScale(5, BigDecimal.ROUND_CEILING)+"");
            txtViewClose.append(" "+stockOut.bdClose.setScale(5, BigDecimal.ROUND_CEILING)+"");
            txtViewHigh.append(" "+stockOut.bdHigh.setScale(5, BigDecimal.ROUND_CEILING)+"");
            txtViewLow.append(" "+stockOut.bdLow.setScale(5, BigDecimal.ROUND_CEILING)+"");
            txtViewValue.append(" "+stockOut.bdValueforPHP.setScale(5, BigDecimal.ROUND_CEILING)+"");
            linearLayout_data.addView(view);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                if(blChanged){
                    setResult(RESULT_OK);
                }
                this.finish();
                overridePendingTransition(R.anim.fab_fade_in, R.anim.anim_show_down);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fab_fade_in, R.anim.anim_show_down);
    }
}


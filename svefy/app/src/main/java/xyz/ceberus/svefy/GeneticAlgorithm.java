package xyz.ceberus.svefy;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class GeneticAlgorithm {

	ArrayList<Company>arrList_Company = new ArrayList<>();
	Portfolio portfolio;
	final static int intMaxGen = 10;
	final static int intMaxStock = 5;
	//ArrayList<Portfolio>arrList_Portfolio_Neglect = new ArrayList<>();
	String TAG = "GA";

	GeneticAlgorithm(ArrayList<Company>arrList_Company){
		this.arrList_Company =arrList_Company;
		portfolio = new Portfolio(10);
	}

	public void generatePortfolio(){
		Portfolio portfolio_A = generateParent();
		int intChange = 0;
		int maxSpouse = 0;

		for(int i = 0; i<intMaxGen; i++){
			Portfolio portfolio_B = generateParent(portfolio_A);

			Portfolio portBestParent;
			while((portBestParent = getFitness(portfolio_A,portfolio_B))==portfolio_A&&maxSpouse<5){
				portfolio_B = generateParent(portfolio_A);
				Log.d(TAG,"parent fitness A: "+portfolio_A.bdPortfolioFitness);
				Log.d(TAG,"parent fitness B: "+portfolio_B.bdPortfolioFitness);
				maxSpouse++;
			}
			maxSpouse = 0;
			//System.out.println("parent fitness A: "+portfolio_A.getPortfolioFitess());
			//System.out.println("-----------------");
			Portfolio[] portfolio_Children = generateChildren(portfolio_A,portfolio_B);
			Portfolio[] portfolio_MutantChildren = generateMutants(portfolio_Children);

			Portfolio portBestChild = getFitness(portfolio_MutantChildren);
			Portfolio portWinner = getFitness(portBestParent,portBestChild,true);

			if(portfolio_A!=portWinner){
				intChange++;
			}

			portfolio_A = portWinner;


		/*	outputPortfolio(portBestParent,"parent");
			outputPortfolio(portBestChild,"child");*/

			/*System.out.println(i+" curr gen ");
			System.out.println("parent fitness: "+portBestParent.bdPortfolioFitness);
			System.out.println("child fitness: "+portBestChild.bdPortfolioFitness);
			System.out.println("winner fitness: "+portWinner.bdPortfolioFitness);

			System.out.println("portfolio return: "+ portfolio_A.bdPortfolioReturn);
			System.out.println("portfolio variance: "+portfolio_A.bdPortfolioVariance);
			System.out.println("portfolio fitness: "+portfolio_A.bdPortfolioFitness);
			System.out.println("----------------------------------");*/
		}

		portfolio = portfolio_A;

		/*for(Company compOut:portfolio.arrList_Company){
			Log.d(TAG,compOut.strName+" - price "+compOut.getCurrPrice()+" - ave return: "+compOut.getReturnAve().multiply(new BigDecimal(100)).setScale(5, RoundingMode.CEILING)+"% weight: "+portfolio.getWeight(compOut.strCode).multiply(new BigDecimal(100)).setScale(5, RoundingMode.CEILING)+"%");
		}
		Log.d(TAG,"portfolio return: "+ portfolio.bdPortfolioReturn);
		Log.d(TAG,"portfolio variance: "+portfolio.bdPortfolioVariance);
		Log.d(TAG,"portfolio fitness: "+portfolio.bdPortfolioFitness);
		Log.d(TAG,"changes: "+intChange);
*/

	}

	/*private void outputPortfolio(Portfolio portfolio,String strName){
		System.out.println("portfolio: "+strName);
		for(Company compOut:portfolio.arrList_Company){
			System.out.println(compOut.strName+" - price "+compOut.getCurrPrice()+" - ave return: "+compOut.getReturnAve().multiply(new BigDecimal(100)).setScale(5, RoundingMode.CEILING)+"% weight: "+portfolio.getWeight(compOut.strCode).multiply(new BigDecimal(100)).setScale(5, RoundingMode.CEILING)+"%");
		}
		System.out.println("portfolio fitness: "+portfolio.getPortfolioFitess(true));
		System.out.println("-------------------------------------");
	}*/


	private Portfolio getFitness(Portfolio portBestParent, Portfolio portBestChild, boolean b) {
		// TODO Auto-generated method stub
		Portfolio portA = portBestParent;
		Portfolio portB = portBestChild;

		if(portA.bdPortfolioFitness.doubleValue()>=portB.bdPortfolioFitness.doubleValue()){
			return portA;
		}
		else{
			return portB;
		}
	}

	private Portfolio getFitness(Portfolio[] portfolio) {
		Portfolio portA = portfolio[0];
		Portfolio portB = portfolio[1];

		if(portA.bdPortfolioFitness.doubleValue()>=portB.bdPortfolioFitness.doubleValue()){
			return portA;
		}
		else{
			return portB;
		}
	}

	private Portfolio getFitness(Portfolio portA, Portfolio portB) {
		// TODO Auto-generated method stub
		if(portA.bdPortfolioFitness.doubleValue()>=portB.bdPortfolioFitness.doubleValue())
			return portA;
		else
			return portB;
	}

	private Portfolio[] generateMutants(Portfolio[] portfolio_Children) {
		// TODO Auto-generated method stub
		Portfolio portA = portfolio_Children[0];
		Portfolio portB = portfolio_Children[1];
		int indexA = getRandom(portA.getSize());
		int indexB = getRandom(portB.getSize());
		while(indexB==indexA){
			indexB = getRandom(portB.getSize());
		}
		Company compTempA = portA.get(indexA);
		Company compTempB = portA.get(indexB);
		portA.set(indexA, compTempB);
		portA.set(indexB, compTempA);

		Company compTempC = portB.get(indexA);
		Company compTempD = portB.get(indexB);
		portB.set(indexA, compTempD);
		portB.set(indexB, compTempC);

		portA.computePortfolioFitness();
		portB.computePortfolioFitness();

		Portfolio[] portfolio_Mutate = {portA,portB};
		return portfolio_Mutate;
	}

	private Portfolio[] generateChildren(Portfolio portfolio_A, Portfolio portfolio_B) {
		// TODO Auto-generated method stub
		Portfolio portfolio_ch1 = new Portfolio(portfolio_A.getSize());
		for(int i = 0; i<portfolio_A.getSize(); i++){
			if(getRandom(1)==1){
				portfolio_ch1.add(portfolio_A.get(i));
			}else{
				portfolio_ch1.add(portfolio_B.get(i));
			}
		}
		Portfolio portfolio_ch2 = new Portfolio(portfolio_A.getSize());
		for(int i = 0; i<portfolio_A.getSize(); i++){
			if(i<portfolio_A.getSize()/2){
				portfolio_ch2.add(portfolio_A.get(i));
			}else{
				portfolio_ch2.add(portfolio_B.get(i));
			}

		}
		Portfolio[] arrPort = {portfolio_ch1,portfolio_ch2};
		return arrPort;
	}



	private Portfolio generateParent() {
		int size = arrList_Company.size();

		Portfolio portfolio = new Portfolio(10);
		/*while(!arrList_Portfolio.contains(comp)){
			arrList_Portfolio.add(comp);
			comp = arrList_Company.get(getRandom(size));
		}
		 */
		while(portfolio.arrList_Company.size()<5){
			Company comp = arrList_Company.get(getRandom(size));
			if(!portfolio.arrList_Company.contains(comp)){
				portfolio.arrList_Company.add(comp);
			}
		}
		portfolio.computePortfolioFitness();
		return portfolio;
	}

	private Portfolio generateParent(Portfolio portfolio_A) {
		// TODO Auto-generated method stub
		int size = arrList_Company.size();

		Portfolio portfolio = new Portfolio(10);
		/*while(!arrList_Portfolio.contains(comp)){
			arrList_Portfolio.add(comp);
			comp = arrList_Company.get(getRandom(size));
		}
		 */
		while(portfolio.arrList_Company.size()<5){
			Company comp = arrList_Company.get(getRandom(size));
			if(!portfolio.arrList_Company.contains(comp)&&!portfolio_A.arrList_Company.contains(comp)){
				portfolio.arrList_Company.add(comp);
			}
		}
		portfolio.computePortfolioFitness();
		return portfolio;
	}



	static int getRandom(int max){
		return new Random().nextInt(max);
	}




}

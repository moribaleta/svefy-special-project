package xyz.ceberus.svefy;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.math.RoundingMode;

import static xyz.ceberus.svefy.MainActivity.portfolio;

public class GraphActivity extends AppCompatActivity {
    GraphView graphWeight;
    GraphView graphReturn;
    ExpandableLayout expandableLayoutWeight;
    ExpandableLayout expandableLayoutReturn;
    TextView textViewWeight;
    TextView textViewReturn;
    ScrollView scrollView;
    CircularProgressView circularProgressView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        expandableLayoutWeight = (ExpandableLayout)findViewById(R.id.expandable_layout_1);
        expandableLayoutReturn = (ExpandableLayout)findViewById(R.id.expandable_layout_2);
        textViewWeight = (TextView)findViewById(R.id.textViewWeight);
        textViewWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayoutWeight.toggle();
            }
        });
        textViewReturn = (TextView)findViewById(R.id.textViewReturn);
        textViewReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayoutReturn.toggle();
            }
        });
        graphWeight = (GraphView)findViewById(R.id.graphWeight);
        graphReturn = (GraphView)findViewById(R.id.graphReturn);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        circularProgressView = (CircularProgressView) findViewById(R.id.progress_view);
        circularProgressView.startAnimation();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            processGraphData();
                            scrollView.setVisibility(View.VISIBLE);
                            circularProgressView.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }).start();
    }

    private void processGraphData() {
        //--------------------------graph weight
        DataPoint[] arrDataPoint = new DataPoint[portfolio.getSize()];
        DataPoint[] arrDataPointReturn = new DataPoint[portfolio.getSize()];
        int i = 0;
        for(Company comp: portfolio.arrList_Company){
            Log.d("Graph",comp.strCode);
            arrDataPoint[i] = new DataPoint(
                    i,portfolio.getWeight(comp.strCode).doubleValue()*100
            );
            arrDataPointReturn[i] = new DataPoint(
                    i,comp.getReturnAve().setScale(2, RoundingMode.CEILING).doubleValue()
            );
            i++;
        }


        BarGraphSeries<DataPoint> series = new BarGraphSeries<DataPoint>(arrDataPoint);
        graphWeight.getViewport().setScalable(true);
        graphWeight.getViewport().setScalableY(true);
        graphReturn.getViewport().setScalable(true);
        graphReturn.getViewport().setScalableY(true);
        graphWeight.addSeries(series);
        graphWeight.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return portfolio.arrList_Company.get((int)value).strCode;
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });

        series.setSpacing(10);
// draw values on top
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.BLACK);
        //--------------------------graph weight

        //--------------------------graph return


        BarGraphSeries<DataPoint> seriesReturn = new BarGraphSeries<DataPoint>(arrDataPointReturn);


        graphReturn.addSeries(seriesReturn);
        /*series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });*/
        seriesReturn.setSpacing(10);
        seriesReturn.setDrawValuesOnTop(true);
        seriesReturn.setValuesOnTopColor(Color.BLACK);
        seriesReturn.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });
        graphReturn.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return portfolio.arrList_Company.get((int)value).strCode;
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });
        //--------------------------graph return
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.fab_fade_in, R.anim.anim_show_down);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fab_fade_in, R.anim.anim_show_down);
    }

}

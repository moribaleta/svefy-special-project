package xyz.ceberus.svefy;

import java.math.BigDecimal;

public class Stock {

	String strCode;
	BigDecimal bdBid;
	BigDecimal bdAsk;
	BigDecimal bdOpen;
	BigDecimal bdHigh;
	BigDecimal bdLow;
	BigDecimal bdClose;
	int intVolume;
	BigDecimal bdVolume;
	BigDecimal bdValueforPHP;
	String strDate;
	Stock(String strCode,
		  String string,
		  String string2,
		  String string3,
		  String string4,
		  String string5,
		  String string6,
		  int intVolume,
		  String string7,
		  String string8,
		  String strDate){
		this.strCode =strCode;
		this.bdBid = new BigDecimal(string);
		this.bdAsk = new BigDecimal(string2);
		this.bdOpen = new BigDecimal(string3);
		this.bdHigh = new BigDecimal(string4);
		this.bdLow = new BigDecimal(string5);
		this.bdClose = new BigDecimal(string6);
		this.intVolume = intVolume;
		this.bdVolume = new BigDecimal(string7);
		this.bdValueforPHP = new BigDecimal(string8);
		this.strDate = strDate;
	}

}

package xyz.ceberus.svefy;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;

/**
 * Created by Eli on 2/15/2017.
 */

public class CustomViewPager extends ViewPager {

    boolean blDisableTouch = false;

    public CustomViewPager(Context context) {
        super(context);
    }

    public void Disable(Boolean blDisableTouch){
        this.blDisableTouch = blDisableTouch;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (blDisableTouch) {

            return true;
        } else {
            return super.onTouchEvent(event);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return super.onInterceptTouchEvent(event);
    }
}

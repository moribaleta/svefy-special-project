package xyz.ceberus.svefy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.jjoe64.graphview.GraphView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

//import com.github.aakira.expandablelayout.ExpandableRelativeLayout;


public class MainPortfolioInfo extends Fragment {

    private static final String TAG = "MAINPORTFOLIO";
    DBHandler dbHandler;
    GraphView graphWeight;
    GraphView graphReturn;
    Portfolio portfolio;
    LinearLayout linearLayout;
    View view;
    //ProgressBar progressBar;
    //ProgressWheel wheel;
    CircularProgressView progressView;
    ScrollView scrollView;
    ExpandableLayout expandableLayout;
    public MainPortfolioInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_main_portfolio_info, container, false);
        dbHandler = new DBHandler(getContext());
        dbHandler.open();
        //listView = (ListView)view.findViewById(R.id.listview_portfolio);
        graphWeight = (GraphView)view.findViewById(R.id.graphWeight);
        graphReturn = (GraphView)view.findViewById(R.id.graphReturn);
        //progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressView= (CircularProgressView)view.findViewById(R.id.progress_view);
        progressView.startAnimation();
        scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        TextView textViewGraph = (TextView)view.findViewById(R.id.textViewGraph);
        textViewGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(portfolio!=null) {
                    getActivity().startActivityForResult(new Intent(getActivity(), GraphActivity.class), 1);
                    getActivity().overridePendingTransition(R.anim.anim_show_up,R.anim.anim_show_null);
                }
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    portfolio = dbHandler.getPortfolio();
                    dbHandler.close();
                    MainActivity.portfolio = portfolio;
                    if(portfolio.arrList_Company.size()>0) {
                        portfolio.computePortfolioFitness();
                        linearLayout = (LinearLayout) view.findViewById(R.id.linear_layout_main);
                        ArrayList<String> arrayList = new ArrayList<>();
                        for (Company company : portfolio.arrList_Company) {
                            arrayList.add(company.strName);
                        }
                        synchronized (this) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "ended");
                                    processPortfolioInfo(view);
                                    //processGraphData();
                                    processStockData();
                                    scrollView.setVisibility(View.VISIBLE);
                                    //progressBar.setVisibility(View.GONE);
                                    progressView.stopAnimation();
                                    progressView.setVisibility(View.GONE);
                                }
                            });
                        }
                    }else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "ended");
                                scrollView.setVisibility(View.VISIBLE);
                                progressView.stopAnimation();
                                progressView.setVisibility(View.GONE);
                            }
                        });
                    }
                }catch (Exception e){
                    Log.e(TAG,e.getMessage());
                }
            }
        }).start();

        //listView.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,arrayList));



        return view;
    }

    private void processPortfolioInfo(View view) {
        TextView textViewReturn = (TextView)view.findViewById(R.id.textViewReturn);
        TextView textViewValue = (TextView)view.findViewById(R.id.textViewNetValue);
        TextView textViewVariance = (TextView)view.findViewById(R.id.textViewVariance);
        TextView textViewAvePrice = (TextView)view.findViewById(R.id.textViewPriceShare);
        TextView textViewVolume = (TextView)view.findViewById(R.id.textViewVolume);
        textViewReturn.append(portfolio.getPortfolioReturn().setScale(3, RoundingMode.CEILING).doubleValue()*100+"%");

        BigDecimal bdValue = new BigDecimal(0);
        double dbAvePrice = 0;
        for(Company compOut: portfolio.arrList_Company){
            Log.d(TAG,compOut.strCode+": "+compOut.getCurrPrice().doubleValue()+"");
            try {
                bdValue = bdValue.add(compOut.getCurrPrice().multiply(new BigDecimal(10)).setScale(3, BigDecimal.ROUND_CEILING));
                dbAvePrice += compOut.getCurrPrice().doubleValue();
            }catch (ArithmeticException e){

            }
        }
        textViewValue.append(bdValue+"");
        textViewVariance.append(portfolio.getPortfolioVariance().setScale(3,RoundingMode.CEILING).doubleValue()+"");
        textViewAvePrice.append(new BigDecimal(dbAvePrice/portfolio.getSize()).setScale(3,BigDecimal.ROUND_CEILING)+"");
        textViewVolume.append(portfolio.getSize()*10+"");
    }

   /* private void processGraphData() {
        //--------------------------graph weight
        DataPoint[] arrDataPoint = new DataPoint[portfolio.getSize()];
        int i = 0;
        for(Company comp: portfolio.arrList_Company){
            Log.d(TAG,comp.strCode);
            arrDataPoint[i] = new DataPoint(
                  i,portfolio.getWeight(comp.strCode).doubleValue()*100
            );
            i++;
        }
        MainActivity.viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(expandableLayout.isExpanded()){
                    MainActivity.viewPager.beginFakeDrag();
                    return true;
                }else {
                    MainActivity.viewPager.endFakeDrag();
                    return false;
                }

            }
        });


        BarGraphSeries<DataPoint> series = new BarGraphSeries<DataPoint>(arrDataPoint);
        graphWeight.getViewport().setScalable(true);
        graphWeight.getViewport().setScalableY(true);
        graphWeight.addSeries(series);
        graphWeight.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return portfolio.arrList_Company.get((int)value).strCode;
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });

        series.setSpacing(10);
// draw values on top
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.BLACK);
        //--------------------------graph weight

        //--------------------------graph return
        DataPoint[] arrDataPointReturn = new DataPoint[portfolio.getSize()];
        int j = 0;
        for(Company comp: portfolio.arrList_Company){
            Log.d(TAG,comp.strCode);
            arrDataPointReturn[j] = new DataPoint(
                    j+1,comp.getReturnAve().setScale(2,RoundingMode.CEILING).doubleValue()
            );
            j++;
        }



        BarGraphSeries<DataPoint> seriesReturn = new BarGraphSeries<DataPoint>(arrDataPointReturn);
        graphReturn.addSeries(seriesReturn);
        *//*series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });*//*
        seriesReturn.setSpacing(10);
        seriesReturn.setDrawValuesOnTop(true);
        seriesReturn.setValuesOnTopColor(Color.BLACK);
        //--------------------------graph return
    }*/

    private void processStockData(){

        LayoutInflater li = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (final Company company: portfolio.arrList_Company){
            View view = li.inflate(R.layout.stockdata_portfolio, null);
            final ExpandableLayout expandableLayout
                    = (ExpandableLayout) view.findViewById(R.id.expandable_layout);
            expandableLayout.collapse();
            TextView textViewName= (TextView)view.findViewById(R.id.textViewName);
            TextView textViewPrice = (TextView)view.findViewById(R.id.textViewPrice);
            TextView textViewValue = (TextView)view.findViewById(R.id.textViewValue);
            TextView textViewReturn = (TextView)view.findViewById(R.id.textViewReturn);
            TextView textViewWeight = (TextView)view.findViewById(R.id.textViewWeight);
            Button btnView = (Button)view.findViewById(R.id.btnView);
            textViewName.setText(company.strName);
            textViewPrice.append(company.getCurrPrice().doubleValue()+"");
            textViewValue.append(company.getCurrPrice().multiply(new BigDecimal(10)).setScale(3,BigDecimal.ROUND_CEILING)+"");
            textViewReturn.append(company.getReturnAve().multiply(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_CEILING)+"%");
            textViewWeight.append(portfolio.getWeight(company.strCode).multiply(new BigDecimal(100)).setScale(3,RoundingMode.CEILING)+"%");
            textViewName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // expand
                    Log.d(TAG,"expanded?: "+expandableLayout.isExpanded());
                    expandableLayout.toggle();
                }
            });
            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(),CompanyActivity.class);
                    intent.putExtra("Company_Code",company.strCode);
                    getActivity().startActivityForResult(intent,1);
                    getActivity().overridePendingTransition(R.anim.anim_show_up,R.anim.anim_show_null);
                }
            });
            linearLayout.addView(view);
        }

    }


    // TODO: Rename method, update argument and hook method into UI event

}

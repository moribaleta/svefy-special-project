package xyz.ceberus.svefy;

import android.content.Context;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by Eli on 1/29/2017.
 */

/**
 * Created by Eli on 1/29/2017.
 */
public class Company {
    String strCode;
    String strName;
    String strCategory;
    ArrayList<Stock>arrList_Stock;
    Context context;

    Company(String strCode,String strName,String strCategory,Context context){
        this.strCode = strCode;
        this.strName = strName;
        this.strCategory = strCategory;
        this.context = context;
        //arrList_Stock = new DBhandler().getStockQuotes(strCode);
        DBHandler dbHandler = new DBHandler(context);
        dbHandler.open();
        arrList_Stock = dbHandler.getStockDataFrom(strCode);
        dbHandler.close();
    }

    public BigDecimal getCurrPrice() {
        DBHandler dbHandler = new DBHandler(context);
        dbHandler.open();
        BigDecimal bigDecimal = dbHandler.getCurrStock(strCode).bdClose;
        dbHandler.close();
        return  bigDecimal;
    }

    public BigDecimal getReturn(int i){
        BigDecimal bdReturn = new BigDecimal(0);
        try{
            bdReturn = (arrList_Stock.get(i).bdClose.subtract(arrList_Stock.get(i-1).bdClose)).divide(arrList_Stock.get(i-1).bdClose);
        }catch(ArithmeticException e){

        }
        return bdReturn;
    }

    public BigDecimal getReturnAve() {
        // TODO Auto-generated method stub
        //System.out.println("company: "+strCode);

        BigDecimal bdTotalReturn = new BigDecimal(0);
        bdTotalReturn.setScale(5, RoundingMode.CEILING);
        for(int i = arrList_Stock.size()-1; i > 0 ; i--){
            //	System.out.println(arrList_Stock.get(i).dbClose +" - "+arrList_Stock.get(i-1).dbClose);
            BigDecimal bdTemp = new BigDecimal(0);
            try{
                bdTemp = (
                        arrList_Stock.get(i).bdClose.subtract(arrList_Stock.get(i-1).bdClose))
                        .divide(arrList_Stock.get(i-1).bdClose,MathContext.DECIMAL32);
            }catch(ArithmeticException e){

            }
            bdTotalReturn = bdTotalReturn.add(bdTemp);
        }
        //System.out.println("returnAve: "+bdTotalReturn);

        bdTotalReturn = bdTotalReturn.divide(new BigDecimal(arrList_Stock.size()-1),5, RoundingMode.HALF_UP);

        return bdTotalReturn;
    }

    public BigDecimal getVariance(){
        int totalSize = arrList_Stock.size();
        BigDecimal bdCoeff = new BigDecimal(0);
        for(Stock stockOut:arrList_Stock){
            bdCoeff = bdCoeff.add(stockOut.bdClose);
        }
        bdCoeff = bdCoeff.divide(new BigDecimal(totalSize));

        BigDecimal bdValue = new BigDecimal(0);
        for(Stock stockOut: arrList_Stock){
            bdValue = bdValue.add(
                    (stockOut.bdClose
                            .subtract(bdCoeff)).pow(2));
        }

        return bdValue.divide(new BigDecimal(totalSize), MathContext.DECIMAL32);
    }

}


package xyz.ceberus.svefy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Eli on 1/29/2017.
 */

public class DBHandler extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "StockDB.db";
    private static final String TAG = "DBHandler";
    private static String PATH;

    SQLiteDatabase sqlDatabase;
    Context context;

    public DBHandler(Context context) {
        super(context, DATABASE_NAME , null, 1);
        PATH = context.getFilesDir().getPath();
        this.context = context;
        createDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDatabase() {
        boolean exists =  checkDatabase();

        Log.v(TAG, exists ? "Database is valid" : "Database is not found or invalid");
        if (!exists) {
            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {
                copyDatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDatabase() {
        SQLiteDatabase checkDB = null;

        try{
            String path = PATH + DATABASE_NAME;
            Log.v(TAG, "Checking " + DATABASE_NAME);
            checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){
            Log.e(TAG, "checkDatabase", e);
        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDatabase() throws IOException {
        //Open your local db as the input stream
        Log.v(TAG, "Copying Database");
        InputStream myInput = context.getAssets().open(DATABASE_NAME);

        // Path to the just created empty db
        String outFile = PATH + DATABASE_NAME;

        //Open the empty db as the output stream
        OutputStream os = new FileOutputStream(outFile);

        //transfer bytes from the input file to the output file
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            os.write(buffer, 0, length);
        }

        //Close the streams
        os.flush();
        os.close();
        myInput.close();
    }

    public void open(){

        //Open the database
        String myPath = PATH + DATABASE_NAME;
        Log.v(TAG, "Opening " + DATABASE_NAME);
        sqlDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close() {
        Log.v(TAG, "Closing database");
        if(sqlDatabase != null)
            sqlDatabase.close();
        super.close();
    }


    public void savePortfolio(Portfolio portfolio){
        for(Company company: portfolio.arrList_Company){
            try {
                /*String sql = "insert into PORTFOLIO_TABLE (Company_Code) values('" + company.strCode + "')";
                sqlDatabase.execSQL(sql);
                */
                ContentValues values = new ContentValues();
                values.put("Company_Code",company.strCode);
                long newRow = sqlDatabase.insert("PORTFOLIO_TABLE",null, values);
                Log.d(TAG,"adding "+company.strCode+" affected: "+newRow);
            }catch (SQLiteException e){
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Company> getAllCompanies(){
        ArrayList<Company>arrList_Company = new ArrayList<>();
        String sql = "select * from COMPANY_TABLE";
        Cursor cursor = sqlDatabase.rawQuery(sql,null);
        if (!cursor.moveToFirst()) {
            Log.d(TAG, "Couldn't move to first");
            // do something here if move failed
        }else{
            Log.d(TAG, "moved to first");
            Log.d(TAG, "cursor row: "+cursor.getCount());
        }
        while (!cursor.isAfterLast()){
            arrList_Company.add(
                    new Company(
                            cursor.getString(cursor.getColumnIndex("Company_Code")),
                            cursor.getString(cursor.getColumnIndex("Company_Name")),
                            cursor.getString(cursor.getColumnIndex("Company_Category")),
                            context
                    )
            );
            cursor.moveToNext();
        }
        cursor.close();
        return  arrList_Company;
    }

    public ArrayList<String> getCategory(){
        ArrayList<String>arrList_Category = new ArrayList<>();
        String sql = "select distinct Company_Category from COMPANY_TABLE";
        Cursor cursor = sqlDatabase.rawQuery(sql,null);
        if (!cursor.moveToFirst()) {
            Log.d(TAG, "Couldn't move to first");
            // do something here if move failed
        }else{
            Log.d(TAG, "moved to first");
            Log.d(TAG, "cursor row: "+cursor.getCount());
        }
        while (!cursor.isAfterLast()){
            arrList_Category.add(
                            cursor.getString(cursor.getColumnIndex("Company_Category"))
            );
            cursor.moveToNext();
        }
        cursor.close();
        return  arrList_Category;
    }


    public Company getCompany(String strCode){

        String sql = "select * from COMPANY_TABLE where Company_Code='"+strCode+"'";
        Cursor cursor = sqlDatabase.rawQuery(sql,null);
        cursor.moveToFirst();
        Company company =  new Company(
                cursor.getString(cursor.getColumnIndex("Company_Code")),
                cursor.getString(cursor.getColumnIndex("Company_Name")),
                cursor.getString(cursor.getColumnIndex("Company_Category")),
                context
        );
        cursor.close();
        return company;
    }

    public ArrayList<Company> getAllCompanies(String strCategory){
        ArrayList<Company>arrList_Company = new ArrayList<>();
        String sql = "select * from COMPANY_TABLE where Company_Category LIKE '%"+strCategory+"%' " ;
        Cursor cursor = sqlDatabase.rawQuery(sql,null);
        if (!cursor.moveToFirst()) {
            Log.d(TAG, "Couldn't move to first");
            // do something here if move failed
        }else{
            Log.d(TAG, "moved to first");
            Log.d(TAG, "cursor row: "+cursor.getCount());
        }
        while (!cursor.isAfterLast()){
            arrList_Company.add(
                    new Company(
                            cursor.getString(cursor.getColumnIndex("Company_Code")),
                            cursor.getString(cursor.getColumnIndex("Company_Name")),
                            cursor.getString(cursor.getColumnIndex("Company_Category")),
                            context
                    )
            );
            cursor.moveToNext();
        }
        cursor.close();
        return  arrList_Company;
    }

    public ArrayList<Stock>getStockDataFrom(String strCode){

        ArrayList<Stock>arrayList_Stock = new ArrayList<>();

        String sql = "select * from QUOTES_TABLE where Company_Code='"+strCode+"' ORDER BY Quotes_Date";
        Cursor cursor = sqlDatabase.rawQuery(sql,null);
        if (!cursor.moveToFirst()) {
            Log.d(TAG, "Couldn't move to first");
            // do something here if move failed
        }else{
            Log.d(TAG, "moved to first");
            Log.d(TAG, "cursor row: "+cursor.getCount());
        }
        while (!cursor.isAfterLast()){
            arrayList_Stock.add(
                    new Stock(
                            strCode,
                            cursor.getString(cursor.getColumnIndex("Bid")),
                            cursor.getString(cursor.getColumnIndex("Ask")),
                            cursor.getString(cursor.getColumnIndex("Open")),
                            cursor.getString(cursor.getColumnIndex("Quotes_High")),
                            cursor.getString(cursor.getColumnIndex("Quotes_Low")),
                            cursor.getString(cursor.getColumnIndex("Quotes_Close")),
                            cursor.getInt(cursor.getColumnIndex("Quotes_Volume")),
                            cursor.getString(cursor.getColumnIndex("Quotes_Value")),
                            cursor.getString(cursor.getColumnIndex("Quotes_Value_Foreign")),
                            cursor.getString(cursor.getColumnIndex("Quotes_Date"))
                    )
            );
            Log.d(TAG,"cursor: "+strCode+
                    cursor.getDouble(cursor.getColumnIndex("Bid"))+
                    cursor.getDouble(cursor.getColumnIndex("Ask"))+
                    cursor.getDouble(cursor.getColumnIndex("Open"))+
                    cursor.getDouble(cursor.getColumnIndex("Quotes_High"))+
                    cursor.getDouble(cursor.getColumnIndex("Quotes_Low"))+
                    cursor.getDouble(cursor.getColumnIndex("Quotes_Close"))+
                    cursor.getInt(cursor.getColumnIndex("Quotes_Volume"))+
                    cursor.getDouble(cursor.getColumnIndex("Quotes_Value"))+
                    cursor.getDouble(cursor.getColumnIndex("Quotes_Value_Foreign"))+
                    cursor.getString(cursor.getColumnIndex("Quotes_Date")));
            cursor.moveToNext();
        }
        cursor.close();
        return  arrayList_Stock;
    }

    public Stock getCurrStock(String strCode){
        Stock stock = null;

            //Class.forName("org.sqlite.JDBC");
            //connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Eli\\Desktop\\mori_files\\special project\\StockDB.db");
            //statement = connection.createStatement();
            //rs = statement.executeQuery("select * from QUOTES_TABLE where Company_Code = '"+strCode+"' order by QUOTES_DATE DESC LIMIT 1");
            String sql = "select * from QUOTES_TABLE where Company_Code = '"+strCode+"' order by QUOTES_DATE DESC LIMIT 1";
            Cursor cursor = sqlDatabase.rawQuery(sql,null);
            cursor.moveToFirst();
            stock = new Stock(
                    strCode,
                    cursor.getString(cursor.getColumnIndex("Bid")),
                    cursor.getString(cursor.getColumnIndex("Ask")),
                    cursor.getString(cursor.getColumnIndex("Open")),
                    cursor.getString(cursor.getColumnIndex("Quotes_High")),
                    cursor.getString(cursor.getColumnIndex("Quotes_Low")),
                    cursor.getString(cursor.getColumnIndex("Quotes_Close")),
                    cursor.getInt(cursor.getColumnIndex("Quotes_Volume")),
                    cursor.getString(cursor.getColumnIndex("Quotes_Value")),
                    cursor.getString(cursor.getColumnIndex("Quotes_Value_Foreign")),
                    cursor.getString(cursor.getColumnIndex("Quotes_Date"))
            );

        cursor.close();


        return stock;
    }

    public Portfolio getPortfolio() {
        String sql = "select C.Company_Code, C.Company_Name, C.Company_Category from COMPANY_TABLE C JOIN PORTFOLIO_TABLE P ON C.Company_Code = P.Company_Code";
        Cursor c = sqlDatabase.rawQuery(sql,null);
        c.moveToFirst();
        Portfolio portfolio = new Portfolio(10);
        while (!c.isAfterLast()){
            portfolio.arrList_Company.add(
                    new Company(
                            c.getString(c.getColumnIndex("Company_Code")),
                            c.getString(c.getColumnIndex("Company_Name")),
                            c.getString(c.getColumnIndex("Company_Category")),
                            context
                    )
            );
            c.moveToNext();
        }
        return portfolio;
    }

    public void saveToPortfolio(String strCode) {
        try {
                /*String sql = "insert into PORTFOLIO_TABLE (Company_Code) values('" + company.strCode + "')";
                sqlDatabase.execSQL(sql);
                */
            ContentValues values = new ContentValues();
            values.put("Company_Code",strCode);
            long newRow = sqlDatabase.insert("PORTFOLIO_TABLE",null, values);
            Log.d(TAG,"adding "+strCode+" affected: "+newRow);
        }catch (SQLiteException e){
            e.printStackTrace();
        }
    }

    public HashMap<String,String> searchCompany(String strValue){
        strValue = strValue.toUpperCase();
        HashMap<String,String>hashMap = new HashMap<>();
        String sql = "select * from COMPANY_TABLE where Company_Code like '%"+strValue+"%' OR Company_Name like '%"+strValue+"%' ORDER BY 2";
        Cursor c = sqlDatabase.rawQuery(sql,null);
        c.moveToFirst();

        while (!c.isAfterLast()){
            hashMap.put(
                    c.getString(c.getColumnIndex("Company_Name")),
                    c.getString(c.getColumnIndex("Company_Code"))
            );
            c.moveToNext();
        }
        c.close();
        return hashMap;
    }

    public void deleteFromPortfolio(String strCode) {
        try {
                /*String sql = "insert into PORTFOLIO_TABLE (Company_Code) values('" + company.strCode + "')";
                sqlDatabase.execSQL(sql);
                */
            /*ContentValues values = new ContentValues();
            values.put("Company_Code",strCode);
            //long newRow = sqlDatabase.delete("PORTFOLIO_TABLE",null, values);
            long newRow = sqlDatabase.delete("PORTFOLIO_TABLE","Company_Code = "+strCode,null);*/
            String sql = "delete from PORTFOLIO_TABLE where Company_Code = '"+strCode+"'";
            sqlDatabase.execSQL(sql);
            //Log.d(TAG,"adding "+strCode+" affected: "+newRow);
        }catch (SQLiteException e){
            e.printStackTrace();
        }
    }
}

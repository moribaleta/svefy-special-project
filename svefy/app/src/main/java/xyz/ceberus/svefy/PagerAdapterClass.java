package xyz.ceberus.svefy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Eli on 11/16/2016.
 */
public class PagerAdapterClass extends FragmentPagerAdapter {
    int mNumOfTabs;

    public PagerAdapterClass(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MainPortfolioInfo mainPortfolioInfo = new MainPortfolioInfo();
                return mainPortfolioInfo;
            case 1:
                MainStockData mainStockData = new MainStockData();
                return mainStockData;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

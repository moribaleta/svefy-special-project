package xyz.ceberus.svefy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

public class PortfolioActivity extends AppCompatActivity {
    DBHandler dbHandler;
    Spinner spinner;
    String TAG = "PortfolioActivity";
    CircularProgressView progressView;
    LinearLayout linearLayout;
    Portfolio portfolioFinal;
    Boolean blRefresh = false;
    ArrayList<Company>arrayList_Company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_portfolio);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinner = (Spinner)findViewById(R.id.spinner);
        dbHandler = new DBHandler(this);
        dbHandler.open();

        ArrayList<String>arrayList_Category = dbHandler.getCategory();
        dbHandler.close();

        progressView= (CircularProgressView)findViewById(R.id.progress_view);
        spinner.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, arrayList_Category));
        linearLayout = (LinearLayout)findViewById(R.id.linear_layout);

        FloatingActionButton fab =(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(portfolioFinal!=null){
                    dbHandler.open();
                    dbHandler.savePortfolio(portfolioFinal);
                    Snackbar.make(view, "added to portfolio", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    blRefresh = true;
                    dbHandler.close();
                    setResult(RESULT_OK,null);
                    finish();
                }
            }
        });
    }

    public void generateStock(View view){
        progressView.startAnimation();
        dbHandler.open();
        String strCategory = spinner.getSelectedItem().toString();
        arrayList_Company = dbHandler.getAllCompanies(strCategory);
        String[]arrStringCompany = {"HOLDING FIRMS","PROPERTY","BANKS","FOOD,BEVERAGE&TOBACCO","MINING","INDUSTRIAL","TRANSPORTATION SERVICES"};
        if(arrayList_Company.size()<=10){
            arrayList_Company.addAll(dbHandler.getAllCompanies(arrStringCompany[new Random().nextInt(arrStringCompany.length)]));
            Snackbar.make(view, "added extra companies because the selected category is below the minimum genetic pool", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

        for(Company compOut: arrayList_Company){
            Log.d(TAG,"company: "+compOut.strName);
        }
        dbHandler.close();

        new Thread(new Runnable() {
            Portfolio portfolio;
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(1000);
                        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm(arrayList_Company);
                        geneticAlgorithm.generatePortfolio();
                        portfolio = geneticAlgorithm.portfolio;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processStockData(portfolio);
                                progressView.stopAnimation();
                                progressView.setVisibility(View.GONE);
                            }
                        });
                        portfolioFinal = portfolio;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }
    /*public void createList(Portfolio portfolio){

        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try{
            for(Company companyOut:portfolio.arrList_Company){
                View view1 = li.inflate(R.layout.stockdata_portfolio,null);
                processPortfolioInfo(view1,portfolio,companyOut);
                *//*TextView textView = (TextView)view1.findViewById(android.R.id.text1);
                textView.setText(companyOut.strName);
                Log.d(TAG,"runnable company: "+companyOut.strName);*//*
                linearLayout.addView(view1);
            }
            progressView.stopAnimation();
            progressView.setVisibility(View.GONE);
        }catch (Exception e){
            Log.d(TAG,e.getMessage());
        }
    }*/

    private void processStockData(Portfolio portfolio){

        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (final Company company: portfolio.arrList_Company){
            View view = li.inflate(R.layout.stockdata_portfolio, null);
            final ExpandableLayout expandableLayout
                    = (ExpandableLayout) view.findViewById(R.id.expandable_layout);
            expandableLayout.collapse();
            TextView textViewName= (TextView)view.findViewById(R.id.textViewName);
            TextView textViewPrice = (TextView)view.findViewById(R.id.textViewPrice);
            TextView textViewValue = (TextView)view.findViewById(R.id.textViewValue);
            TextView textViewReturn = (TextView)view.findViewById(R.id.textViewReturn);
            TextView textViewWeight = (TextView)view.findViewById(R.id.textViewWeight);
            Button btnView = (Button)view.findViewById(R.id.btnView);
            textViewName.setText(company.strName);
            textViewPrice.append(company.getCurrPrice().doubleValue()+"");
            textViewValue.append(company.getCurrPrice().multiply(new BigDecimal(10)).setScale(3,BigDecimal.ROUND_CEILING)+"");
            textViewReturn.append(company.getReturnAve().multiply(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_CEILING)+"%");
            textViewWeight.append(portfolio.getWeight(company.strCode).multiply(new BigDecimal(100)).setScale(3,RoundingMode.CEILING)+"%");
            textViewName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // expand
                    Log.d(TAG,"expanded?: "+expandableLayout.isExpanded());
                    expandableLayout.toggle();
                }
            });
            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(PortfolioActivity.this,CompanyActivity.class);
                    intent.putExtra("Company_Code",company.strCode);
                    overridePendingTransition(R.anim.anim_show_up,R.anim.fab_fade_out);
                    startActivityForResult(intent,1);
                }
            });
            linearLayout.addView(view);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            blRefresh = true;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                if(blRefresh){
                    setResult(RESULT_OK,null);
                }
                this.finish();
                overridePendingTransition(R.anim.anim_show_up, R.anim.anim_show_down);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fab_fade_in, R.anim.anim_show_down);
    }

}

package xyz.ceberus.svefy;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.ArrayList;
import java.util.HashMap;

import static xyz.ceberus.svefy.MainActivity.fab;

public class MainStockData extends Fragment {

    public MainStockData() {
        // Required empty public constructor
    }

    ListView listView_Stock;
    //ProgressBar progressBar;
    CircularProgressView progressView;
    DBHandler dbHandler;
    ArrayList<String>arrayList_String;
    EditText editTextSearch;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_stock_data, container, false);
        listView_Stock = (ListView)v.findViewById(R.id.listview_stock);
        progressView= (CircularProgressView)v.findViewById(R.id.progress_view);
        progressView.startAnimation();
        editTextSearch = (EditText)v.findViewById(R.id.editText);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        setList();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        return v;
    }

    public void search(){

        String strValue = editTextSearch.getText().toString();
        if(strValue.length()>=1) {
            dbHandler.open();
            final HashMap<String, String> hashMap = dbHandler.searchCompany(strValue);
            dbHandler.close();
            ArrayList<String> arrayList = new ArrayList<>(hashMap.keySet());
            listView_Stock.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arrayList));

            listView_Stock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getActivity(), CompanyActivity.class);
                    intent.putExtra("Company_Code", hashMap.get(listView_Stock.getItemAtPosition(i).toString()));
                    getActivity().startActivityForResult(intent, 1);
                    getActivity().overridePendingTransition(R.anim.anim_show_up,R.anim.anim_show_null);
                }
            });
        }

    }

    public void setList(){
        dbHandler = new DBHandler(getContext());
        dbHandler.open();
        final ArrayList<Company> arrayList_Company = dbHandler.getAllCompanies();
        arrayList_String =  new ArrayList<>();
        for(Company company: arrayList_Company){
            arrayList_String.add(company.strName);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listView_Stock.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,arrayList_String));
                listView_Stock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String strCode = arrayList_Company.get(i).strCode;
                        Intent intent = new Intent(getActivity(),CompanyActivity.class);
                        intent. putExtra("Company_Code",strCode);
                        //startActivity(intent);
                        startActivityForResult(intent,1);
                        getActivity().overridePendingTransition(R.anim.anim_show_up,R.anim.anim_show_null);
                    }
                });
                dbHandler.close();
                progressView.stopAnimation();
                progressView.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    search();
                }
            });
            setIcon(true);
        }else{
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(),PortfolioActivity.class);
                    //startActivity(new Intent(MainActivity.this,PortfolioActivity.class));
                    getActivity().startActivityForResult(intent,1);
                    getActivity().overridePendingTransition(R.anim.anim_show_up,R.anim.anim_show_null);
                }
            });
            setIcon(false);
        }

    }

    public void setIcon(final Boolean blSearch){
        fab.playHideAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fab.playShowAnimation();
                if(blSearch){
                    fab.setImageResource(R.drawable.ic_search_white_24dp);
                }else{
                    fab.setImageResource(R.drawable.ic_add_white_24dp);
                }
            }
        },300);
    }
}
